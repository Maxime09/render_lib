pub use sdl2 as sdl;

pub struct Window{
    sdl: sdl2::Sdl,
    video_subsystem: sdl2::VideoSubsystem,
    window: sdl2::video::Window,
    _context: sdl2::video::GLContext
}

impl Window{
    pub fn new(title: &str, width: u32, height: u32) -> Self{
        let sdl = sdl2::init().unwrap();
        let video_subsystem = sdl.video().unwrap();

        let gl_attr = video_subsystem.gl_attr();
        gl_attr.set_context_profile(sdl2::video::GLProfile::Core);
        gl_attr.set_context_version(3, 3);
        gl_attr.set_double_buffer(true);

        let window = video_subsystem.window(title, width, height)
            .opengl()
            .resizable()
            .build()
            .unwrap();

        let context = window.gl_create_context().unwrap();

        Window{
            sdl,
            video_subsystem,
            window,
            _context: context
        }
    }

    pub fn init_gl(&self) -> gl::Gl{
        gl::Gl::load_with(|s| self.video_subsystem.gl_get_proc_address(s) as *const std::os::raw::c_void)
    }

    pub fn get_events(&self) -> sdl2::EventPump{
        self.sdl.event_pump().unwrap()
    }

    pub fn swap_window(&self){
        self.window.gl_swap_window();
    }

}
