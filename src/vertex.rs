use crate::render_gl::data;

pub trait VertexTrait{
    fn vertex_attrib_pointers(gl: &gl::Gl);
}

/// Represent a vertex in 3D space
#[derive(VertexAttribPointers, Copy, Clone, Debug)]
#[repr(C, packed)]
pub struct Vertex {
    /// The position of the vertex
    #[location = 0]
    pub pos: data::f32_f32_f32,
    /// The color of the vertex using 3 color channels (red, blue and green) and an alpha channel.
    #[location = 1]
    pub clr: data::u2_u10_u10_u10_rev_float,
}

impl Vertex {
    /// Create a single vertex
    ///
    /// # Arguments
    ///
    /// * `x`, `y` and `z` - representing respectively the x, y and z coordinates of the vertex
    ///
    /// * `r`, `g` and `b` - representing respectively the red, green and blue color channels of the color of the vertex
    ///
    /// * `a` - representing the alpha channel of the color of the vertex
    ///
    /// # Example
    ///
    /// ```
    /// use render_lib::Vertex;
    ///
    /// // Create red vertex at the coordinates (1.0, 2.0, 3.0)
    /// let vertex = Vertex::new(1.0, 2.0, 3.0, 1.0, 0.0, 0.0, 1.0);
    /// ```
    pub fn new(x: f32, y: f32, z: f32, red: f32, green: f32, blue: f32, alpha: f32) -> Self{
        Vertex{
            pos: (x, y, z).into(),
            clr: (red, green, blue, alpha).into(),
        }
    }

    /// Create a single vertex
    ///
    /// # Arguments
    ///
    /// * `pos` - representing the coordinates of the vertex
    ///
    /// * `clr` - representing color of the vertex with rgba format
    ///
    /// # Example
    ///
    /// ```
    /// use render_lib::Vertex;
    ///
    /// let pos = (1.0, 2.0, 3.0);
    /// let color = (1.0, 0.0, 0.0, 1.0);
    ///
    /// // Create red vertex at the coordinates (1.0, 2.0, 3.0)
    /// let vertex = Vertex::new(pos, color);
    /// ```
    pub fn from_tuple(pos: (f32, f32, f32), clr: (f32, f32, f32, f32)) -> Self {
        Vertex{
            pos: pos.into(),
            clr: clr.into(),
        }
    }
}
