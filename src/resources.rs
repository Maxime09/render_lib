use std::path::{Path, PathBuf};
use std::fs;
use std::io::Read;
use std::ffi;

/* #[derive(Debug, Fail)]
pub enum Error{
    #[fail(display = "I/O error")]
    Io(#[cause] io::Error),
    #[fail(display = "I/O error")]
    FileContainsNil,
    #[fail(display = "Failed get executable path")]
    FailedToGetExePath,
}

impl From<io::Error> for Error {
    fn from(other: io::Error) -> Self{
        Error::Io(other)
    }
}
*/

pub struct Resources {
    root_path: PathBuf,
}

impl Resources {
    pub fn from_relative_exe_path(rel_path: &Path) -> Resources {
        let exe_file_name = ::std::env::current_exe()
            .unwrap();
        let exe_path = exe_file_name.parent()
            .unwrap();
        Resources { root_path: exe_path.join(rel_path) }
    }

    pub fn load_cstring(&self, resource_name: &str) -> ffi::CString {
        let mut file = fs::File::open(
            resource_name_to_path(&self.root_path, resource_name)
        ).unwrap();

        let mut buffer: Vec<u8> = Vec::with_capacity(
            file.metadata().unwrap().len() as usize + 1
        );
        file.read_to_end(&mut buffer).unwrap();

        if buffer.iter().find(|i| **i == 0).is_some() {
            panic!("File contains Nil");
        }

        unsafe {ffi::CString::from_vec_unchecked(buffer)}
    }
}

fn resource_name_to_path(root_dir: &Path, location: &str) -> PathBuf {
    let mut path: PathBuf = root_dir.into();

    for part in location.split('/'){
        path = path.join(part);
    }

    path
}
