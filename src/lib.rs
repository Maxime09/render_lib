/*!
# render_lib
This is a library to help writing program using GL.

## How to use it:

*/
#[macro_use] extern crate render_gl_derive;
extern crate vec_2_10_10_10;

pub mod render_gl;
pub mod resources;
pub mod vertex;
#[doc(hidden)]
//pub mod debug;
mod time_counter;
pub mod object_3d;
pub mod projection;
mod window;

pub use nalgebra as na;

pub use gl;
pub use sdl2 as sdl;
pub use object_3d::Object3D as Object3D;
pub use vertex::Vertex as Vertex;

pub use resources::Resources as Resources;
pub use time_counter::TimeCounter as TimeCounter;
pub use render_gl::Camera as Camera;
pub use render_gl::Program as Program;
pub use render_gl::Viewport as Viewport;
pub use projection::Projection as Projection;
pub use window::Window;
pub use vertex::VertexTrait as VertexTrait;


use std::ffi::CString;

pub fn calculate_mvp_matrix<T>(projection: &Projection, camera: &Camera, object: &Object3D<T>) -> na::Matrix4<f32> where T: VertexTrait + Clone{
    let proj = projection.get_projection_matrix();
    let view = camera.get_view_matrix();
    let model = object.get_model_matrix();
    proj.to_homogeneous() * view.to_homogeneous() * model
}

pub fn enable_z_buffer(gl: &gl::Gl){
    unsafe{
        gl.Enable(gl::DEPTH_TEST);
        gl.DepthFunc(gl::LESS);
    }
}

pub fn render_object<T>(object: &Object3D<T>, mvp: &na::Matrix4<f32>, program: &Program, mvp_id: gl::types::GLint, gl: &gl::Gl) where T: VertexTrait + Clone{
    let mvp_pointer = mvp.as_slice().as_ptr();
    unsafe{
        gl.UniformMatrix4fv(mvp_id, 1, gl::FALSE, mvp_pointer);
    }
    object.render(program, gl);
}

pub fn render<T>(objects: &[Object3D<T>], projection: &Projection, camera: &Camera, mvp_name: &CString, program: &Program, gl: &gl::Gl) where T: VertexTrait + Clone{
    let mvp_id = program.get_uniform_location(gl, mvp_name.clone());
    let vp = projection.get_projection_matrix().to_homogeneous() * camera.get_view_matrix().to_homogeneous();
    for object in objects{
        let mvp = vp * object.get_model_matrix();
        render_object(&object, &mvp, program, mvp_id, gl);
    }
}
