use std::time::Instant;

pub struct TimeCounter{
    start: Instant
}

impl TimeCounter{
    pub fn new() -> Self{
        TimeCounter {
            start: Instant::now()
        }
    }

    pub fn reset(&mut self){
        self.start = Instant::now();
    }

    pub fn get_elapsed(&self) -> f32{
        (self.start.elapsed().as_micros() as f32) / 1000000.0
    }

    pub fn get_delta_time(&mut self) -> f32{
        let delta = self.get_elapsed();
        self.reset();
        delta
    }
}


impl Default for TimeCounter{
    fn default() -> Self {
        Self::new()
    }
}
