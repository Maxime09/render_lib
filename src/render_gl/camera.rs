use nalgebra as na;

#[derive(Clone)]
pub struct Camera{
    pos: na::Point3<f32>,
    target: na::Point3<f32>,
}

impl Camera {
    pub fn new(pos_x: f32, pos_y: f32, pos_z: f32, target_x: f32, target_y: f32, target_z: f32) -> Self{
        Camera {
            pos: na::Point3::new(pos_x, pos_y, pos_z),
            target: na::Point3::new(target_x, target_y, target_z),
        }
    }

    pub fn new_look_at_origin(x: f32, y: f32, z: f32) -> Self{
        Self::new(x, y, z, 0.0, 0.0, 0.0)
    }

    pub fn from_vector(pos: na::Vector3<f32>, target: na::Vector3<f32>) -> Self{
        Camera{
            pos: na::Point3::from_homogeneous(pos.fixed_resize::<na::U4, na::U1>(1.0)).unwrap(),
            target: na::Point3::from_homogeneous(target.fixed_resize::<na::U4, na::U1>(1.0)).unwrap(),
        }
    }

    pub fn from_vector_look_at_origin(pos: na::Vector3<f32>) -> Self{
        Self::from_vector(pos, na::zero())
    }

    pub fn from_points(pos: na::Point3<f32>, target: na::Point3<f32>) -> Self {
        Camera{
            pos,
            target
        }
    }

    pub fn from_point(pos: na::Point3<f32>) -> Self {
        Self::from_points(pos, na::Point3::new(0.0, 0.0, 0.0))
    }

    pub fn get_view_matrix(&self) -> na::Isometry3<f32> {
        na::Isometry3::look_at_rh(&self.pos, &self.target, &na::Vector3::y())
    }

    pub fn translate(&mut self, vector: na::Vector3<f32>, delta_time: f32){
        let t = na::Isometry3::new(vector * delta_time, na::zero());
        self.pos = t * self.pos;
    }

    pub fn translate_keeping_orientation(&mut self, vector: na::Vector3<f32>, delta_time: f32) {
        let t = na::Isometry3::new(vector * delta_time, na::zero());
        self.pos = t * self.pos;
        self.target = t * self.target;
    }

    pub fn set_pos(&mut self, pos: na::Point3<f32>){
        self.pos = pos;
    }

    pub fn get_pos(&self) -> na::Point3<f32>{
        self.pos
    }

    pub fn set_pos_keeping_orientation(&mut self, pos: na::Point3<f32>){
        let diff = self.target - self.pos;
        self.pos = pos;
        self.target = pos + diff;
    }

    pub fn set_target(&mut self, target: na::Point3<f32>){
        self.target = target;
    }

    pub fn get_target(&self) -> na::Point3<f32>{
        self.target
    }
}
