use crate::render_gl::{self, buffer};

pub use crate::vertex::Vertex;
pub use crate::vertex::VertexTrait;

use nalgebra as na;

/// Represent a 3D Object composed of one or several triangles
pub struct Object3D<T> where T: VertexTrait + Clone{
    #[doc(hidden)]
    _vbo: buffer::ArrayBuffer,
    #[doc(hidden)]
    vao: buffer::VertexArray,
    #[doc(hidden)]
    vertices: Vec<T>,
    vertices_len: usize,

    pos: na::Vector3<f32>,
    scale: na::Vector3<f32>,
    rotation: na::Vector3<f32>
}

impl<T> Object3D<T> where T: VertexTrait + Clone{
    /// Create a new 3D objects from its vertices
    ///
    /// # Arguments
    ///
    /// * `vertices` - a reference to a vector containing each vertices of the object.
    /// The vertices a grouped by 3 to make a triangle.
    /// The number of vertices should be a multiple of 3
    ///
    /// * `gl` - should be a reference to the gl object
    ///
    /// # Example
    ///
    /// ```
    /// extern crate gl;
    /// use render_lib::{Object3D, Vertex};
    ///
    /// let gl = ....
    /// let Vertices = vec![
    ///     Vertex {
    ///         ...
    ///     }, ...
    /// ];
    /// let object = Object3D::new(&vertices, &gl)
    /// ```
    pub fn new(vertices: &[T], gl: &gl::Gl) -> Object3D<T>{

        // set up vertex buffer object

        let vbo = buffer::ArrayBuffer::new(gl);
        vbo.bind();
        vbo.static_draw_data(vertices);
        vbo.unbind();

        // set up vertex array object

        let vao = buffer::VertexArray::new(gl);

        vao.bind();
        vbo.bind();
        T::vertex_attrib_pointers(gl);
        vbo.unbind();
        vao.unbind();

        Object3D{
            _vbo: vbo,
            vao,
            vertices: vertices.to_owned(),
            vertices_len: vertices.len(),
            pos: na::zero(),
            scale: na::Vector3::new(1.0, 1.0, 1.0),
            rotation: na::zero()
        }
    }


    /// Render a 3D object
    ///
    /// # Arguments
    ///
    /// * `program` - a reference to a shader program
    ///
    /// * `gl` - should be a reference to the gl object
    ///
    /// # Example
    ///
    /// ```
    /// extern crate gl;
    /// use render_lib::{Object3D, Program}
    ///
    /// let gl = ....
    /// let object = Object3D::new(....);
    /// let program = Program::from_res(....);
    ///
    /// ....
    ///
    /// object.render(&program, &gl);
    /// ```
    pub fn render(&self, program: &render_gl::Program, gl: &gl::Gl) {
        program.set_used();
        self.vao.bind();

        unsafe {
            gl.DrawArrays(
                gl::TRIANGLES, //mode
                0,              // starting index
                self.vertices_len as i32 // number of vertices
            );
        }

        self.vao.unbind();

    }

    pub fn get_pos(&self) -> na::Vector3<f32>{
        self.pos
    }

    pub fn get_scale(&self) -> na::Vector3<f32>{
        self.scale
    }

    pub fn get_rotation(&self) -> na::Vector3<f32>{
        self.rotation
    }

    pub fn set_pos(&mut self, pos: na::Vector3<f32>){
        self.pos = pos;
    }

    pub fn set_scale(&mut self, scale: na::Vector3<f32>){
        self.scale = scale;
    }

    pub fn set_rotation(&mut self, rotation: na::Vector3<f32>){
        self.rotation = rotation;
    }

    pub fn get_model_matrix(&self) -> na::Matrix4<f32>{
        let scale_mat = na::Matrix4::new(                       // scale matrix
            self.scale.x,   0.0,            0.0,            0.0,
            0.0,            self.scale.y,   0.0,            0.0,
            0.0,            0.0,            self.scale.z,   0.0,
            0.0,            0.0,            0.0,            1.0,
        );
        let rot_mat = na::Rotation3::from_euler_angles(to_rad(self.rotation.x), to_rad(self.rotation.z), to_rad(self.rotation.y));
        let translation_mat = na::Translation3::new(self.pos.x, self.pos.y, self.pos.z);
        translation_mat.to_homogeneous() * rot_mat.to_homogeneous() * scale_mat
    }

    pub fn clone(&self, gl: &gl::Gl) -> Self{
        let mut obj = Object3D::new(&self.vertices, gl);
        obj.set_rotation(self.get_rotation());
        obj.set_scale(self.get_scale());
        obj.set_pos(self.get_pos());
        obj
    }

}

fn to_rad(degree: f32) -> f32{
    degree * (std::f32::consts::PI / 180.0)
}
