use crate::Viewport;
use nalgebra as na;

pub struct Projection {
    matrix: na::Perspective3<f32>
}

impl Projection{
    pub fn new(width: i32, height: i32, fovy: f32, near: f32, far: f32, gl: &gl::Gl) -> Self{
        let port = Viewport::for_window(width, height);
        port.set_used(&gl);
        Projection{
            matrix: na::Perspective3::new(port.get_aspect_ratio(), fovy, near, far)
        }
    }

    pub fn get_projection_matrix(&self) -> na::Perspective3<f32>{
        self.matrix
    }

    pub fn set_aspect(&mut self, width: i32, height: i32, gl: &gl::Gl){
        let port = Viewport::for_window(width, height);
        port.set_used(&gl);
        self.matrix.set_aspect(port.get_aspect_ratio());
    }

    pub fn set_fovy(&mut self, field_of_view_y: f32){
        self.matrix.set_fovy(field_of_view_y);
    }

    pub fn set_near_clip(&mut self, near: f32){
        self.matrix.set_znear(near);
    }

    pub fn set_far_clip(&mut self, far: f32){
        self.matrix.set_zfar(far);
    }
}
