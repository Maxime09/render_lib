mod bindings {
    include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
}

pub use bindings::*;
pub use bindings::Gl as InnerGl;

use std::sync::Arc;

#[derive(Clone)]
pub struct Gl {
    inner: Arc<bindings::Gl>,
}

impl Gl {
    pub fn load_with<F>(loadfn: F) -> Gl
        where F: FnMut(&'static str) -> *const types::GLvoid
    {
        Gl {
            inner: Arc::new(bindings::Gl::load_with(loadfn))
        }
    }
}

use std::ops::Deref;

impl Deref for Gl {
    type Target = bindings::Gl;

    fn deref(&self) -> &bindings::Gl {
        &self.inner
    }
}

unsafe impl Send for Gl {}
unsafe impl Sync for Gl {}
